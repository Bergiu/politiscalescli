// License and Copyright notice:
// Copyright (c) 2017 8values. http://8values.github.io.
// https://github.com/RadicaliseesSurInternet/politiscales/blob/master/LICENSE
// Changes are relicensed under GPLv3

var qn = 0; // Question number
var prev_answer = null;

rand_c = Math.random();
rand_b = Math.random();
rand_s = Math.random();
rand_j = Math.random();
rand_p = Math.random();
rand_m = Math.random();
rand_e = Math.random();
rand_t = Math.random();

rand_per_axis = {
    c0: rand_c,
    c1: 1 - rand_c,
    b0: rand_b,
    b1: 1 - rand_b,
    s0: rand_s,
    s1: 1 - rand_s,
    j0: rand_j,
    j1: 1 - rand_j,
    p0: rand_p,
    p1: 1 - rand_p,
    m0: rand_m,
    m1: 1 - rand_m,
    e0: rand_e,
    e1: 1 - rand_e,
    t0: rand_t,
    t1: 1 - rand_t,
};

questions = [
    /* ESSENTIALISME ************************************************ */
    {
        question: "&quot;Man wird nicht als Frau geboren, sondern man wird zu einer.&quot;",
        question_en: "“One is not born, but rather becomes, a woman.”",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Unterschiede im Umgang mit verschiedenen Gruppen in unserer Gesellschaft sowie in deren Lebensqualität zeigen, dass Rassismus in unserer Gesellschaft immer noch allgegenwärtig ist.",
        question_en: "Differences of treatement and quality of life in our society show that racism is still omnipresent.",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Alle Wissenschaften, sogar Chemie und Biologie, sind nicht absolut, sondern werden von unserer Gesellschaft bestimmt.",
        question_en: "All sciences, even chemistry and biology, are not absolute and are subject to being shaped by our society.",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Die Kategorien &quot;Mann&quot; und &quot;Frau&quot; sind soziale Konstrukte, die aufgegeben werden sollten.",
        question_en: "The categories “women” and “men” are social constructs that should be given up.",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Niemand ist von Natur aus zur Kriminalität geneigt.",
        question_en: "Nobody is by nature predisposed to criminality.",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Sexuelle Orientierung ist ein soziales Konstrukt.",
        question_en: "Sexual orientation is a social construct",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Soziale Unterschiede zwischen ethnischen Gruppen lassen sich nicht biologisch erklären.",
        question_en: "Social differences between ethnic groups cannot be explained by biology.",
        answer: 0,
        valuesYes: [
            {
                axis: "c0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c1",
                value: 3
            }
        ]
    },
    {
        question: "Die sozialen Rollen von Mann und Frau in unserer Gesellschaft können teilweise biologisch erklärt werden.",
        question_en: "The social roles of women and men can partly be explained by biological differences.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ]
    },
    {
        question: "Hormonelle Unterschiede können manche individuellen Charakterunterschiede zwischen Frauen und Männern erklären.",
        question_en: "Hormonal differences can explain some differences in individual characteristics between women and men.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ]
    },
    {
        question: "Sexuelle Übergriffe können zum Teil durch die natürlichen Triebe von Männern erklärt werden.",
        question_en: "Sexual assaults are partly caused by men&#39;s natural impulse.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ]
    },
    {
        question: "Transgender Individuen werden nie wirklich das Geschlecht sein, dass sie gerne wären.",
        question_en: "Transgender individuals will never really be of the gender they would like to be.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            }
        ]
    },
    {
        question: "Mitglieder einer Nation oder Kultur haben unveränderliche Charakteristiken, die sie ausmachen.",
        question_en: "Members of a nation or culture have some unchangeable characteristics that define them.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            }
        ]
    },
    {
        question: "Biologisch gesehen ist die menschliche Spezies für Heterosexualität gemacht.",
        question_en: "Biologically, human beings are designed for heterosexuality.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            }
        ]
    },
    {
        question: "Unabhängig vom Kontext ist Egoismus der Hauptantrieb der menschlichen Spezies.",
        question_en: "Selfishness is the overriding drive in the human species, no matter the context.",
        answer: 0,
        valuesYes: [
            {
                axis: "c1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "c0",
                value: 3
            }
        ]
    },
    /* NATIONALISME ************************************************* */
    {
        question: "Grenzen sollten irgendwann abgeschafft werden.",
        question_en: "Borders should eventually be abolished.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Menschen sollten für ihre Ideale einstehen, selbst wenn das dazu führt, dass sie ihr Land verraten.",
        question_en: "People need to stand up for their ideals, even if it leads them to betray their country.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Mein Land muss für die Schäden zahlen, die durch die Verbrechen verursacht worden sind, die es im Ausland begangen hat.",
        question_en: "My country must pay for the damages caused by the crimes it commited in other countries.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Wenn zwei Länder eine Wirtschaft, soziale Systeme, und ähnliche Umweltnormen teilen, stellt ein freier Handel zwischen ihnen keine Probleme dar.",
        question_en: "If two countries have similar economies, social systems and environmental norms, then the free market between them has no negative impact.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Nationalchauvinismus ist bei sportlichen Wettkämpfen inakzeptabel.",
        question_en: "National Chauvinism during sport competitions is not acceptable.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Ich bin gleichermaßen besorgt um die Bewohner meines Landes und der Bewohner anderer Länder.",
        question_en: "I am equally concerned about the inhabitants of my country and those of other the countries.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Ausländer, die in meinem Land leben, sollten politisch genauso handeln können, wie Menschen mit Staatsangehörigkeit.",
        question_en: "Foreigners living in my country should be allowed to act politically, equally to those who have the nationality.",
        answer: 0,
        valuesYes: [
            {
                axis: "b0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b1",
                value: 3
            }
        ]
    },
    {
        question: "Staatsbürger sollten gegenüber Ausländern Priorität haben.",
        question_en: "Citizens should take priority over foreigners.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Die Werte meines Landes sind denen anderer Länder überlegen.",
        question_en: "The values of my country are superior to those of other countries.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Multikulturalismus ist eine Bedrohung für unsere Gesellschaft.",
        question_en: "Multiculturalism is a threat to our society.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Ein guter Bürger ist ein Patriot.",
        question_en: "A good citizen is a patriot.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Es ist legitim, dass ein Land militärisch im Ausland eingreift, um seine wirtschaftlichen Interessen zu verteidigen.",
        question_en: "It is legitimate for a country to intervene militarily to defend its economic interests.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Geschichte sollte auf solch eine Weise unterrichtet werden, dass ein Gefühl der Zugehörigkeit zur Nation geschaffen wird.",
        question_en: "It is necessary to teach history in order to create a sense of belonging to the nation.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    {
        question: "Forschungen meines Landes sollten anderen Ländern nicht verfügbar sein.",
        question_en: "Research produced by my country should not be available to other countries.",
        answer: 0,
        valuesYes: [
            {
                axis: "b1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "b0",
                value: 3
            }
        ]
    },
    /* PRODUCTION *************************************************** */
    {
        question: "Niemand sollte davon reich werden, ein Geschäft, Häuser, oder Land zu besitzen.",
        question_en: "No one should get rich from owning a business, housing, or land.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Löhne von Privatunternehmen sind eine Art des Diebstahls von den Arbeitern.",
        question_en: "Wage labour is a form of theft from the worker by companies.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Es ist wichtig, dass Gesundheit eine Angelegenheit des Staates bleibt.",
        question_en: "It is important that health should stay a public matter.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Energie und Transport/Mobilität sollten eine öffentliche Angelegenheit sein.",
        question_en: "Energy and transport structures should be a public matter.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Patente sollten nicht existieren.",
        question_en: "Patents should not exist.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Es ist wichtig, Versammlungen zu haben, die unsere Produktion den Bedürfnissen der Verbraucher anpasst.",
        question_en: "It is necessary to implement assemblies to ration our production to the consumers according to their needs.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Der Arbeitsmarkt versklavt die Arbeiter.",
        question_en: "The labor market enslave workers.",
        answer: 0,
        valuesYes: [
            {
                axis: "p0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p1",
                value: 3
            }
        ]
    },
    {
        question: "Suche nach dem eigenen persönlichen Profit ist gesund für die Wirtschaft.",
        question_en: "Looking for one&#39;s own profit is healthy for the economy.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Leistungsunterschiede erklären unterschiedlichen Wohlstand zwischen zwei Personen.",
        question_en: "It is merit that explains differences of wealth between two individuals.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Dass manche Schulen und Universitäten privat sind ist kein Problem.",
        question_en: "The fact that some schools and universities are private is not a problem.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Betriebsverlagerungen sind ein notwendiges Übel, um Produktion zu steigern.",
        question_en: "Offshoring and outsourcing are necessary evils to improve production.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Es ist in Ordnung, dass es reiche und arme Menschen gibt.",
        question_en: "It is acceptable that there are rich and poor people.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Industriesektoren in privater Hand sind in Ordnung.",
        question_en: "It is acceptable that some industry sectors are private.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    {
        question: "Banken sollten privat bleiben.",
        question_en: "Banks should remain private.",
        answer: 0,
        valuesYes: [
            {
                axis: "p1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "p0",
                value: 3
            }
        ]
    },
    /* MARKET ******************************************************* */
    {
        question: "Einkommen und Kapital sollten besteuert werden, um Reichtum umzuverteilen.",
        question_en: "Revenues and capital should be taxed to redistribute wealth.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Das Rentenalter sollte gesenkt werden.",
        question_en: "The age of retirement should be lowered.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Willkürliche Entlassungen sollten verboten werden.",
        question_en: "Dismissals of employees should be forbidden except if it is justified.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Es sollte einen Mindestlohn geben, um zu garantieren, dass ein Arbeiter von seiner Arbeit leben kann.",
        question_en: "Minimal levels of salary should be ensured to make sure that a worker can live of her/his work.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Private Monopole müssen vermieden werden.",
        question_en: "It is necessary to avoid private monopoly.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Schulden der öffentlichen Träger (Staaten, Länder, Gemeinden...) müssen nicht unbedingt zurückgezahlt werden.",
        question_en: "Loans contracted in the public sphere (State, regions, collectivities...) should not necessarily be refunded.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Manche Branchen oder Arten von Beschäftigung sollten finanziell unterstützt werden.",
        question_en: "Some sectors or type of employment should be financially supported.",
        answer: 0,
        valuesYes: [
            {
                axis: "m0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m1",
                value: 3
            }
        ]
    },
    {
        question: "Die Marktwirtschaft ist optimal, wenn sie nicht reguliert wird.",
        question_en: "Market economy is optimal when it is not regulated.",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Heutzutage ist ein Arbeitnehmer*in frei Entscheidungen zu treffen, wenn er/sie einen Vertrag mit seinem zukünftigen Arbeitgeber unterschreibt.",
        question_en: "Nowadays employees are free to choose when signing a contract with their future employer",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Es ist notwendig arbeitsrechtliche Regelungen abzuschaffen, damit die Unternehmen angeregt werden, Angestellte einzustellen.",
        question_en: "It is necessary to remove regulations in labour legislation to encourage firms to hire.",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Die legal erlaubte Arbeitszeit sollte erhöht werden.",
        question_en: "The maximum allowed hours in the legal work week should be increased.",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Umweltfreundliche Regeln sollten von dem Massenverbrauch und nicht von einer staatlichen Instanz beeinflusst werden.",
        question_en: "Environmental norms should be influenced by mass consumption and not from an authority.",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Sozialhilfe hält Leute davon ab, zu arbeiten.",
        question_en: "Social assistance deters people from working.",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    {
        question: "Staatseigene Unternehmen sollten verwaltet werden, wie private Unternehmen d. h. nach der Logik des Marktes (Konkurrenz, Rentabilität).",
        question_en: "State-run companies should be managed like private ones and follow the logic of the market (competition, profitability...).",
        answer: 0,
        valuesYes: [
            {
                axis: "m1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "m0",
                value: 3
            }
        ]
    },
    /* SOCIETE ****************************************************** */
    {
        question: "Sitten und Traditionen sollten hinterfragt werden.",
        question_en: "Traditions should be questioned.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Ich habe kein Problem damit, wenn andere Sprachen neben der bisherigen Amtssprache oder statt dieser offizielle Landessprache werden.",
        question_en: "I do not have any problem if other official languages are added or replace the already existing official language in my country.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Heiraten/Ehe sollte abgeschafft werden.",
        question_en: "Marriage should be abolished.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Ausländer bereichern unsere Kultur.",
        question_en: "Foreigners enrich our culture.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Der Einfluss von Religionen sollte verringert werden.",
        question_en: "The influence of religion should decrease.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Eine Sprache wird von deren Nutzern und nicht von Akademikern definiert.",
        question_en: "A language is defined by its users, not by scholars.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Sterbehilfe sollte erlaubt sein.",
        question_en: "Euthanasia should be authorised.",
        answer: 0,
        valuesYes: [
            {
                axis: "s0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s1",
                value: 3
            }
        ]
    },
    {
        question: "Homosexuellen sollten im Vergleich zu Heterosexuellen anders behandelt werden, z. B. Bei der Ehe, Adoption, Zeugung.",
        question_en: "Homosexuals should not be treated equally to heterosexuals with regards to marriage, filiation, adoption or procreation.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            }
        ]
    },
    {
        question: "Die Todesstrafe ist unter bestimmten Umständen gerechtfertigt.",
        question_en: "In some specific conditions, the death penalty is justified.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            },
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            },
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Technischer Fortschritt sollte die Gesellschaft nicht zu schnell verändern.",
        question_en: "Technological progress must not change society too quickly.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            }
        ]
    },
    {
        question: "Die Schule sollte unsere Werte, Traditionen und grundlegendes Wissen lehren.",
        question_en: "School should mostly teach our values, traditions and fundamental knowledge.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            }
        ]
    },
    {
        question: "Abtreiben sollte auf spezifische Fälle begrenzt werden.",
        question_en: "Abortion should be limited to specific cases.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            },
            {
                axis: "femi",
                value: 3
            }
        ]
    },
    {
        question: "Das Hauptziel eines Paars ist mindestens ein Kind zu bekommen.",
        question_en: "The main goal of a couple is to make at least one child.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            }
        ]
    },
    {
        question: "Abstinenz ist Verhütungsmitteln vorzuziehen, um die wahre Natur des Geschlechtsverkehrs zu bewahren.",
        question_en: "Abstinence should be prefered to contraception, to preserve the true nature of the sexual act.",
        answer: 0,
        valuesYes: [
            {
                axis: "s1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "s0",
                value: 3
            }
        ]
    },
    /* ECOLOGIE ***************************************************** */
    {
        question: "Es ist nicht hinnehmbar, dass Tierarten durch menschliche Handlungen aussterben.",
        question_en: "It is not acceptable that human actions should lead to the extinction of species.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Gentechnisch veränderte Organismen sollten - abgesehen von Forschung und medizinischen Zwecken - verboten sein.",
        question_en: "GMOs should be forbidden outside research and medical purposes.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Wir müssen die globale Erderwärmung bekämpfen.",
        question_en: "We must fight against global warming.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Wir sollten Veränderungen unserer Essgewohnheiten akzeptieren, um die Ausbeutung der Natur einzuschränken.",
        question_en: "We should accept changes in our way of consuming food to limit the exploitation of nature.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Es ist wichtig eine umweltfreundliche Landwirtschaft zu fördern, die die Artenvielfalt bewahrt, selbst wenn der Ertrag geringer ist.",
        question_en: "It is important to encourage an agriculture that maintains a food biodiversity, even if the output is inferior.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Die Erhaltung von ländlichen Ökosystemen is wichtiger als Arbeitschaffung.",
        question_en: "Preserving non urban ecosystems is more important than creating jobs.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Die Verringerung von Abfall sollte durch die Verringerung von Produktion erfolgen.",
        question_en: "Reduction of waste should be done by reducing production.",
        answer: 0,
        valuesYes: [
            {
                axis: "e0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e1",
                value: 3
            }
        ]
    },
    {
        question: "Die Weltraumkolonisierung ist eine gute Alternative, um den Mangel an Rohstoffen (Seltene Metalle, Öl, ...) zu überwinden",
        question_en: "Space colonization is a good solution for supplying the lack of raw material on Earth (iron, rare metals, fuel...) ",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Ökosysteme dauerhaft zu verändern, um menschliche Lebensqualität zu verbessern, ist gerechtfertigt.",
        question_en: "Transforming ecosystems durably to increase the quality of life of human beings is legitimate.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Es ist notwendig, massiv in die Forschung zu investieren, um die Produktivität zu erhöhen.",
        question_en: "It is necessary to massively invest in research to improve productivity.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Transhumanismus ist vorteilhaft, denn es erlaubt uns, unser Potenzial zu erweitern.",
        question_en: "Transhumanism will be beneficial because it will allow us to improve our capacities.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Kernspaltung, sofern vernünftig instand gehalten, ist eine gute Energiequelle.",
        question_en: "Nuclear fission, when well maintained, is a good source of energy.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Die Verwertung fossiler Brennstoffe ist notwendig.",
        question_en: "Exploitation of fossil fuels is necessary.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    {
        question: "Ein Ziel der Regierung sollte sein, starkes Wirtschaftswachstum zu erhalten.",
        question_en: "Maintaining strong economic growth should be an objective for the government.",
        answer: 0,
        valuesYes: [
            {
                axis: "e1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "e0",
                value: 3
            }
        ]
    },
    /* LIBERTAIRE *************************************************** */
    {
        question: "Gefängnisse sollten abgeschafft werden.",
        question_en: "Prisons should no longer exist.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Minimalstrafen für Vergehen oder Straftaten sind ungerecht.",
        question_en: "It is unfair to set a minimal penalty for an offense or a crime.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Personen, die aus dem Gefängnis entlassen werden, sollten bei ihrer Wiedereingliederung in die Gesellschaft unterstützt werden.",
        question_en: "Individuals who get out of prison should be accompanied in their reinsertion.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Die Justiz sollte immer den Kontext und die Vorgeschichte des Verurteilten beachten und die Strafe dementsprechend anpassen.",
        question_en: "Justice should always take into consideration the context and the past of the condemned and adapt their penalty accordingly.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Die Haftbedingungen im Gefängnis sollten deutlich verbessert werden.",
        question_en: "Conditions of life in jail should be greatly improved.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Das Speichern persönlicher Daten sollte stark eingeschränkt und der Abgleich verschiedener Datenbanken sollte verboten werden.",
        question_en: "The filing and storage of personal records should be delimited strictly and database cross-checking should be forbidden.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Das Recht, anonym im Internet zu sein, sollte gewährleistet werden.",
        question_en: "The right to be anonymous on Internet should be guaranteed.",
        answer: 0,
        valuesYes: [
            {
                axis: "j0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j1",
                value: 3
            }
        ]
    },
    {
        question: "Der Zweck der Justiz sollte es sein, diejenigen zu bestrafen, die gegen das Gesetz verstoßen.",
        question_en: "The purposeof the judiciary system should be to punish those who went against the law.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Die Polizei sollte bewaffnet sein.",
        question_en: "The police should be armed.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Das Opfern von einigen bürgerlichen Freiheiten ist eine Notwendigkeit, um vor Terroranschlägen zu schützen.",
        question_en: "The sacrifice of some civil liberties is a necessity in order to be protected from terrorist acts.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Ordnung und Autorität sollten unter allen Umständen Respektiert werden.",
        question_en: "Order and authority should be respected in all circumstances.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Schwere Strafen sind effektiv, da sie abschreckend sind.",
        question_en: "Heavy penalties are efficient because they are dissuasive.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    {
        question: "Es ist besser, jemand potenziell Gefährlichen präventiv zu verhaften, anstatt sich der Gefahr auszusetzen, dass er eine Straftat begeht.",
        question_en: "It is better to arrest someone potentially dangerous preventively rather than taking the risk of having them committing a crime.",
        answer: 0,
        valuesYes: [
            {
                axis: "j1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "j0",
                value: 3
            }
        ]
    },
    /* STRATEGIE **************************************************** */
    {
        question: "Ein Generalstreik ist eine guter Weg um neue Rechte zu erwerben.",
        question_en: "Mass strike is a good way to acquire new rights.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Bewaffneter Kampf in einem Land ist manchmal notwendig.",
        question_en: "Armed struggle in a country is sometimes necessary.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Ein Aufstand ist notwendig um die Gesellschaft nachhaltig zu verändern.",
        question_en: "Insurrection is necessary to deeply change society.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Aktivismus in bestehenden politischen Organisationen ist nicht relevant um Gesellschaft zu verändern.",
        question_en: "Activism in existing political organisations is not relevant to change society.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Vom Staat organisierte Wahlen können die Herrschenden nicht hinterfragen.",
        question_en: "Elections organised by the state cannot question the powers in place.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Hacken ist ein legitimes Mittel in politischen Auseinandersetzungen.",
        question_en: "Hacking has a legitimate place in political struggle.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Sabotage ist unter bestimmten Umständen legitim.",
        question_en: "Sabotage is legitimate under certain conditions.",
        answer: 0,
        valuesYes: [
            {
                axis: "t0",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t1",
                value: 3
            }
        ]
    },
    {
        question: "Aktivisten müssen immer in strikter Übereinstimmung mit dem Gesetz handeln.",
        question_en: "Activists must always act in strict accordance with the law.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Revolutionen werden immer schlecht enden.",
        question_en: "Revolutions will always end up in a bad way.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Das System radikal zu verändern, ist kontraproduktiv. Wir sollten es eher schrittweise ändern.",
        question_en: "Changing the system radically is counter-productive. We should rather transform it progressively.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Gewalt gegen Menschen ist niemals sinnvoll.",
        question_en: "Violence against individuals is never productive.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Wir sollten niemals Demonstranten unterstützen, die Gewalt anwenden.",
        question_en: "We should always distance ourselves from protestors who use violence.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Wir müssen Kompromisse mit der Opposition machen, um unsere Ideen umzusetzen.",
        question_en: "We need to make compromises with the opposition to apply our ideas.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    {
        question: "Veränderung im eigenen Leben kann zu Veränderungen in der Gesellschaft führen.",
        question_en: "Changes in an individual&#39;s way of life can induce changes in society.",
        answer: 0,
        valuesYes: [
            {
                axis: "t1",
                value: 3
            }
        ],
        valuesNo: [
            {
                axis: "t0",
                value: 3
            }
        ]
    },
    /* BONUS ******************************************************** */
    {
        question: "Meine Religion muss so weit verbreitet werden wie möglich.",
        answer: 0,
        valuesYes: [
            {
                axis: "reli",
                value: 3
            }
        ],
        valuesNo: []
    },
    {
        question: "Es ist eine kleine Gruppe, die bewusst und heimlich die Welt beherrscht.",
        answer: 0,
        valuesYes: [
            {
                axis: "comp",
                value: 3
            }
        ],
        valuesNo: []
    },
    {
        question: "Eine gute Politik ist pragmatisch und hat keine Ideologie.",
        answer: 0,
        valuesYes: [
            {
                axis: "prag",
                value: 3
            }
        ],
        valuesNo: []
    },
    {
        question: "Wir sollten eine Monarchie etablieren um die Menschen zu verbinden und unsere Souveränität zu schützen.",
        answer: 0,
        valuesYes: [
            {
                axis: "mona",
                value: 3
            }
        ],
        valuesNo: []
    },
    {
        question: "Menschen sollten Tiere weder essen noch ausbeuten.",
        answer: 0,
        valuesYes: [
            {
                axis: "vega",
                value: 3
            }
        ],
        valuesNo: []
    },
    {
        question: "Der Staat sollte abgeschafft werden.",
        answer: 0,
        valuesYes: [
            {
                axis: "anar",
                value: 3
            }
        ],
        valuesNo: []
    }
];

function shuffle(array)
{
    var i = 0 , j = 0 , temp = null;

    for (i = array.length - 1; i > 0; i -= 1)
    {
        j = Math.floor(Math.random() * (i + 1));
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function init_question()
{
    console.log("Frage %num% von %sum%".replace("%num%", (qn + 1)).replace("%sum%", questions.length));
    console.log(questions[qn].question+"\n");
    for (var x in selection) {
        var y = x;
        y++;
        console.log(y+". "+selection[x]);
    }
    if (qn != 0)
    {
        console.log("\n0. Zurück");
    }

}

function next_question(mult) {
    questions[qn].answer = mult;
    qn++;

    if (qn < questions.length)
    {
        init_question();
    }
    else
    {
        results();
    }
}

function prev_question()
{
    if (qn == 0)
    {
        return;
    }
    qn--;
    init_question();

}

function calc_score(score,max_value)
{
    return (100*(score)/(max_value)).toFixed(0);
}

function results()
{
    var axes = {};

    for(var i=0; i<questions.length; i++)
    {
        q = questions[i];

        for(var j=0; j<q.valuesYes.length; j++)
        {
            a = q.valuesYes[j];
            if(!(a.axis in axes))
            {
                axes[a.axis] = {
                    val: 0,
                    sum: 0
                };
            }

            if(q.answer > 0)
            {
                axes[a.axis].val += q.answer * a.value;
            }
            axes[a.axis].sum += Math.max(a.value, 0);
        }

        for(j=0; j<q.valuesNo.length; j++)
        {
            a = q.valuesNo[j];
            if(!(a.axis in axes))
            {
                axes[a.axis] = {
                    val: 0,
                    sum: 0
                };
            }

            if(q.answer < 0)
            {
                axes[a.axis].val -= q.answer * a.value;
            }
            axes[a.axis].sum += Math.max(a.value, 0);
        }
    }

    url = "";
    for(var aK in axes)
    {
        if(axes[aK].val > 0)
        {
            if(url != "")
                url += "&";
            url += aK+"="+calc_score(axes[aK].val, axes[aK].sum);
        }
    }

    url = "/de_DE/results?"+url;
    url = "http://politiscales.net"+url;

    console.log("Dein Ergebnis: "+url);
}

function simulate()
{
    while(qn < questions.length)
    {
        prop = 0.0;
        propCounter = 0;
        for(var j=0; j<questions[qn].valuesYes.length; j++)
        {
            a = questions[qn].valuesYes[j];
            if(a.axis in rand_per_axis)
            {
                prop += rand_per_axis[a.axis];
                propCounter++;
            }
        }
        for(j=0; j<questions[qn].valuesNo.length; j++)
        {
            a = questions[qn].valuesNo[j];
            if(a.axis in rand_per_axis)
            {
                prop += 1.0 - rand_per_axis[a.axis];
                propCounter++;
            }
        }

        if(propCounter > 0)
            prop /= propCounter;
        else
            prop = 0.2;

        if(Math.random() < 0.1)
            next_question(0);
        else if(Math.random() < prop)
            next_question(1);
        else
            next_question(-1);
    }
}

// PolitiScalesCli  Copyright (C) 2019  Bergiu
// Licensed under GPLv3

var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

selection = ["Stimme voll zu", "Stimme etwas zu", "Weder noch", "Eher dagegen", "Vollkommene Ablehnung"];

function main()
{
    shuffle(questions);
    init_question();
    rl.on('line', function(line) {
        var cont = true;
        var prev = false;
        var num;
        switch(line) {
            case "1":
                console.log(selection[0]);
                num = 1;
                break;
            case "2":
                console.log(selection[1]);
                num = 2/3;
                break;
            case "3":
                console.log(selection[2]);
                num = 0;
                break;
            case "4":
                console.log(selection[3]);
                num = -2/3;
                break;
            case "5":
                console.log(selection[4]);
                num = -1;
                break;
            case "0":
                console.log("Zurück!");
                prev_question();
                return;
            default:
                console.log("Ungültige Eingabe.");
                init_question();
                return;
        }
        console.log("");
        if (qn >= questions.length-1) {
            results();
            process.exit();
            return;
        }
        next_question(num);
    });
}

main();
